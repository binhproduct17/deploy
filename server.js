var http = require('http'),
    exec = require('child_process').exec,
    fs = require('fs'),
    LOG = __dirname + '/log',
    SCRIPT = 'sh ' + __dirname + '/deploy.sh';

fs.openSync(LOG, 'a');

http.createServer(function(req, res) {

    if (req.URL === '/deploy') {

        exec([SCRIPT, '>>', LOG, '2>&1'].join(' '));

        res.writeHead(200);
        return res.end('Okay');
    }

    if (req.URL === '/log') {
        res.writeHead(200);
        return fs.createReadStream(LOG).pipe(res);
    }

    res.writeHead(405);
    res.end('Unknown Method');

}).listen(43307);